package br.com.listainkotlinmvp.ui.listMusic

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.listainkotlinmvp.R
import br.com.listainkotlinmvp.data.model.Dado
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_lista.view.*

class ListMusicAdapter(
    private val dados: List<Dado>,
    private val context: Context

) : RecyclerView.Adapter<ListMusicAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.item_lista, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dados.size
    }

    override fun onBindViewHolder(holder: ViewHolder, posicao: Int) {
        val statement = dados[posicao]
        with(holder) {
            nome.text = statement.name
            Glide.with(context).load(statement.im).into(imagem)

            //   posicao.text = statement.name
           // imagem.text = statement.title
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nome = itemView.tv_nome
        val posicao = itemView.tv_posicao
        val imagem = itemView.iv_imagem
        val btDowload = itemView.fb_dowload
    }

}
