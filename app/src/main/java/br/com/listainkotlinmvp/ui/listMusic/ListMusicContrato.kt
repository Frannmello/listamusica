package br.com.listainkotlinmvp.ui.listMusic

import br.com.listainkotlinmvp.data.model.Dado
import com.example.projetobank.util.BasePresenter
import com.example.projetobank.util.BaseView

interface ListMusicContrato {

    interface View: BaseView<Presenter> {
        fun exibeInformacao(mensagem: String)
        fun listarStatement(statement: List<Dado>)
    }
    interface  Presenter: BasePresenter {
        fun baixarArquivo(dado : Dado)
    }
}