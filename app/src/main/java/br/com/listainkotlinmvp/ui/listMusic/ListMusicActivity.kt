package br.com.listainkotlinmvp.ui.listMusic

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.listainkotlinmvp.R
import br.com.listainkotlinmvp.data.source.ObjectRepositorio
import br.com.listainkotlinmvp.data.source.local.AppDataBase
import br.com.listainkotlinmvp.data.source.local.ObjectLocalDataSource
import br.com.listainkotlinmvp.data.source.remote.ObjectRemoteDataSource
import br.com.listainkotlinmvp.util.AppExecutors
import br.com.listainkotlinmvp.util.replaceFragmentInActivity
import com.example.projetobank.ui.home.ListMusicFragment

class ListMusicActivity : AppCompatActivity() {

    private lateinit var presenter: ListMusicPresenter

    companion object {
        const val TAG_USUARIO = "usuario"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_music)
        var data = intent.extras


        val listFragment = supportFragmentManager.findFragmentById(
            R.id.listContentFrame
        ) as ListMusicFragment? ?: ListMusicFragment.newInstance().also {
                replaceFragmentInActivity(it, R.id.listContentFrame)
            }

        val localDB = AppDataBase.getInstance(applicationContext)

        val dadoRemote = ObjectRemoteDataSource.getInstance(
            AppExecutors()
        )
        val dadoLocal = ObjectLocalDataSource.getInstance(
            AppExecutors(),
            localDB.objectDado()
        )

        val repositorio = ObjectRepositorio(dadoLocal,dadoRemote)
        presenter = ListMusicPresenter(repositorio,listFragment)
    }
}
