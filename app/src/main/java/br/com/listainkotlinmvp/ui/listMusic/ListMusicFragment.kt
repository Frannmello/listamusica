package com.example.projetobank.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import br.com.listainkotlinmvp.R
import br.com.listainkotlinmvp.data.model.Dado
import br.com.listainkotlinmvp.data.source.RetrofitInicializador
import br.com.listainkotlinmvp.ui.listMusic.ListMusicAdapter
import br.com.listainkotlinmvp.ui.listMusic.ListMusicContrato
import br.com.listainkotlinmvp.util.TAG_DIALOG
import br.com.listainkotlinmvp.util.pegaFragmentTranscation
import com.example.projetobank.ui.widget.AlertDialogFragment
import kotlinx.android.synthetic.main.fragment_list_music.view.*


class ListMusicFragment : Fragment(), ListMusicContrato.View {

    override lateinit var presenter: ListMusicContrato.Presenter

    private lateinit var root: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        root = inflater.inflate(R.layout.fragment_list_music, container, false)
        return root
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    override fun exibeInformacao(mensagem: String) {
        activity?.let {
            val alertDialogFragment = AlertDialogFragment()
            alertDialogFragment.setMensagem(mensagem)
            alertDialogFragment.setBotaoNeutro("ok") {
                alertDialogFragment.dismiss()
            }
            alertDialogFragment.show(pegaFragmentTranscation(), TAG_DIALOG)
        }
    }

    override fun listarStatement(statement: List<Dado>) {
        val adapter = ListMusicAdapter(statement, requireContext())
        root.rv_lista.adapter = adapter
    }

    override fun configuraUrlRetrofit() {
        RetrofitInicializador.url =
            "https://firebasestorage.googleapis.com/v0/b/desafio-dev-android.appspot.com/"
    }

    companion object {
        @JvmStatic
        fun newInstance() = ListMusicFragment()

    }
}
