package br.com.listainkotlinmvp.ui.listMusic

import android.util.Log
import br.com.listainkotlinmvp.data.model.Dado
import br.com.listainkotlinmvp.data.model.ObjectList
import br.com.listainkotlinmvp.data.source.CallbackResponse
import br.com.listainkotlinmvp.data.source.ObjectRepositorio

class ListMusicPresenter(
    val repositorio: ObjectRepositorio,
    val fragment: ListMusicContrato.View
) : ListMusicContrato.Presenter {

    override fun baixarArquivo(dado: Dado) {
    }

    override fun start() {
        listarArquivosMedia();
    }


    init {
        fragment.presenter = this
    }

    private fun listarArquivosMedia() {
        fragment.configuraUrlRetrofit()
        repositorio.pegarObjects(1, object : CallbackResponse<ObjectList> {
            override fun sucesso(response: ObjectList) {
                fragment.listarStatement(response.minhasMusicas)
            }

            override fun erro() {
                fragment.exibeInformacao("Erro de conexão!")
            }
        })
    }
}