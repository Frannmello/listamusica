package br.com.listainkotlinmvp.data.source

import br.com.listainkotlinmvp.data.source.remote.ObjectService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitInicializador {
    companion object {
        var url: String? = null
    }

    lateinit var retrofit: Retrofit

    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
        client.readTimeout(120, TimeUnit.SECONDS)
        client.connectTimeout(120, TimeUnit.SECONDS)
        client.addInterceptor(interceptor)

        url?.let {
            retrofit = Retrofit.Builder()
                .baseUrl(it)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build()
        }
    }


    fun objectService() = retrofit.create(ObjectService::class.java)

}