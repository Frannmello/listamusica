package br.com.listainkotlinmvp.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import br.com.listainkotlinmvp.data.model.Arquivo

@Dao
interface MediaDao {
    @Query("SELECT * FROM  Arquivo")
    fun pegarMedia(): MutableList<Arquivo>

    @Insert
    fun adicionarDado(dado: Arquivo?)

    @Query("DELETE FROM Arquivo")
    fun deletarDado()
}