
package br.com.listainkotlinmvp.data.source

import br.com.listainkotlinmvp.data.model.Dado
import br.com.listainkotlinmvp.data.model.ObjectList
import br.com.listainkotlinmvp.data.source.remote.ObjectRemoteDataSource

class ObjectRepositorio(
private val localDataSource: ObjectDataSource,
private val remoteDataSource: ObjectRemoteDataSource
) : ObjectDataSource {

    override fun pegarObjects(concentrador: Int, callbackResponse: CallbackResponse<ObjectList>) {
        concentrador.let {
            remoteDataSource.pegarObjects(concentrador, object : CallbackResponse<ObjectList> {
                override fun sucesso(response:ObjectList) {
                    callbackResponse.sucesso(response)
                        salvaDadosMedia(
                             response,
                            {callbackResponse.sucesso(response)},
                            {callbackResponse.erro()}
                        )

                }

                override fun erro() {
                    callbackResponse.erro()
                }
            })
            return
        }
        //localDataSource.pegarObjects(1, callbackResponse)
    }

    override fun salvaDadosMedia(dado:ObjectList, sucesso: () -> Unit, erro: () -> Unit) {
    }


}