package br.com.listainkotlinmvp.data.source


interface CallbackResponse<T> {

    fun sucesso(response: T)
    fun erro()

}