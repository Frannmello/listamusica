package br.com.listainkotlinmvp.data.source.local

import androidx.annotation.VisibleForTesting
import br.com.listainkotlinmvp.data.model.Dado
import br.com.listainkotlinmvp.data.model.ObjectList
import br.com.listainkotlinmvp.data.source.CallbackResponse
import br.com.listainkotlinmvp.data.source.ObjectDataSource
import br.com.listainkotlinmvp.data.source.remote.ObjectRemoteDataSource
import br.com.listainkotlinmvp.util.AppExecutors

class ObjectLocalDataSource(
    val appExecutors: AppExecutors,
    val objectDao: ObjectDao

) : ObjectDataSource {
        override fun pegarObjects(
            concentrador: Int,
            callbackResponse: CallbackResponse<ObjectList>
        ) {
//            appExecutors.diskIO.execute {
//                val dado = objectDao.pegarDado()
//                appExecutors.mainThread.execute {
//                    if (dado.minhasMusicas.isEmpty()) {
//                        callbackResponse.erro()
//                    } else {
//
//                        callbackResponse.sucesso(dado
//                        )
//                    }
//                }
//            }
        }

        override fun salvaDadosMedia(
            musicas:ObjectList,
            sucesso: () -> Unit,
            erro: () -> Unit
        ) {
            appExecutors.diskIO.execute {
                try {
                    objectDao.deletarDado()
                    for (dado in musicas.minhasMusicas) {
                        objectDao.adicionarDado(dado)
                    }

                    appExecutors.mainThread.execute(sucesso)

                } catch (e: Exception) {
                    e.printStackTrace()
                    appExecutors.mainThread.execute(erro)
                }
            }
        }

        companion object {
            private var INSTANCE: ObjectLocalDataSource? = null

            @JvmStatic
            fun getInstance(
                appExecutors: AppExecutors,
                objectDao: ObjectDao
            ): ObjectLocalDataSource {
                if (INSTANCE == null) {
                    synchronized(ObjectLocalDataSource::javaClass) {
                        INSTANCE = ObjectLocalDataSource(appExecutors, objectDao)
                    }
                }
                return INSTANCE!!
            }

            @VisibleForTesting
            fun clearInstance() {
                INSTANCE = null
            }
        }

}