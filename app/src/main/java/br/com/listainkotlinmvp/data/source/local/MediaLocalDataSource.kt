package br.com.listainkotlinmvp.data.source.local

import br.com.listainkotlinmvp.data.model.Arquivo
import br.com.listainkotlinmvp.data.source.CallbackResponse
import br.com.listainkotlinmvp.data.source.MediadataSource
import br.com.listainkotlinmvp.util.AppExecutors

class MediaLocalDataSource (
    val appExecutors: AppExecutors,
    val mediaDao: MediaDao

) : MediadataSource {

    override fun pegarMedia(concentrador: Arquivo?, callbackResponse: CallbackResponse<MutableList<Arquivo>>) {
        appExecutors.diskIO.execute {
            val dado = mediaDao.pegarMedia()
            appExecutors.mainThread.execute {
                if (dado.isEmpty()) {
                    callbackResponse.erro()
                } else {
                    callbackResponse.sucesso(dado)
                }
            }

        }
    }

    override fun salvaMedia(dado: Arquivo, sucesso: () -> Unit, erro: () -> Unit) {
        appExecutors.diskIO.execute {
            try {
                mediaDao.deletarDado()
                mediaDao.adicionarDado(dado)
                appExecutors.mainThread.execute(sucesso)

            } catch (e: Exception){
                e.printStackTrace()
                appExecutors.mainThread.execute(erro)
            }
        }
    }

}