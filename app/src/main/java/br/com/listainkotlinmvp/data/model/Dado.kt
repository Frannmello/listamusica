package br.com.listainkotlinmvp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
class Dado(
    @PrimaryKey()
    @SerializedName("bg")
    val bg: String,
    @SerializedName("im")
    val im: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("sg")
    val sg: String
)