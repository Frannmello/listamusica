package br.com.listainkotlinmvp.data.source.remote

import android.util.Log
import br.com.listainkotlinmvp.data.model.Dado
import br.com.listainkotlinmvp.data.model.ObjectList
import br.com.listainkotlinmvp.data.source.CallbackResponse
import br.com.listainkotlinmvp.data.source.ObjectDataSource
import br.com.listainkotlinmvp.data.source.RetrofitInicializador
import br.com.listainkotlinmvp.data.source.local.ObjectDao
import br.com.listainkotlinmvp.util.AppExecutors
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ObjectRemoteDataSource
    (val appExecutors: AppExecutors)

    : ObjectDataSource {
    override fun pegarObjects(
        concentrador: Int,
        callbackResponse: CallbackResponse<ObjectList>
    ) {
        appExecutors.networkIO.execute {
            concentrador.let { concentrador ->
                RetrofitInicializador()
                    .objectService().requestObject()
                    .enqueue(object : Callback<ObjectList> {
                        override fun onResponse(
                            call: Call<ObjectList>, response: Response<ObjectList>?
                        ) {
                            response?.let {
                                it.body()?.let { retorno ->
                                    appExecutors.mainThread.execute {
                                        callbackResponse.sucesso(retorno)
                                        Log.e("SUCESSO ", " sucesso" + retorno.minhasMusicas[0].name)
                                    }
                                }
                                if (it.body() == null) {
                                    callbackResponse.erro()
                                    Log.e("ERROResposta  NULL", "erro")
                                }
                            }
                        }

                        override fun onFailure(call: Call<ObjectList>?, t: Throwable?) {
                            appExecutors.mainThread.execute {
                                Log.e("ERROResposta ", "erro" + t!!.message)
                                callbackResponse.erro()
                            }
                        }
                    })
            }
        }
    }



    override fun salvaDadosMedia(
        dado:ObjectList,
        sucesso: () -> Unit,
        erro: () -> Unit
    ){

    }

    companion object {
        private lateinit var INSTANCE: ObjectRemoteDataSource
        private var needsNewInstance = true

        @JvmStatic
        fun getInstance(appExecutors: AppExecutors): ObjectRemoteDataSource {
            if (needsNewInstance) {
                INSTANCE = ObjectRemoteDataSource(appExecutors)
                needsNewInstance = false
            }
            return INSTANCE
        }
    }
}