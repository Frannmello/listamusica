package br.com.listainkotlinmvp.data.source.remote

import br.com.listainkotlinmvp.data.model.Dado
import br.com.listainkotlinmvp.data.model.ObjectList
import retrofit2.Call
import retrofit2.http.GET

interface ObjectService {
    @GET("o/assets.json?alt=media&token=964a35bb-53d0-45aa-a3dd-ecad72a2f14c/objects/")
    fun requestObject(): Call<ObjectList>
}