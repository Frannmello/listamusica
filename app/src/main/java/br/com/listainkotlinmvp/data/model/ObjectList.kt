package br.com.listainkotlinmvp.data.model

import com.google.gson.annotations.SerializedName

data class ObjectList(
    @SerializedName("objects")
    val minhasMusicas: List<Dado>
)