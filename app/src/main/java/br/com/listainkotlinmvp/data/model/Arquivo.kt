package br.com.listainkotlinmvp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
class Arquivo(
    @PrimaryKey()
    @SerializedName("arquivoAudio")
    val ArquivoAudio: String,
    @SerializedName("ArquivoVideo")
    val ArquivoVideo: String
) {
}