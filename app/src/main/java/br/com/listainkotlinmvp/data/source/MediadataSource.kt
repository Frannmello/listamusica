package br.com.listainkotlinmvp.data.source

import br.com.listainkotlinmvp.data.model.Arquivo

interface MediadataSource {

    fun pegarMedia(concentrador: Arquivo?, callbackResponse: CallbackResponse<MutableList<Arquivo>>)

    fun salvaMedia(
        dado: Arquivo,
        sucesso: () -> Unit,
        erro: () -> Unit
    ){

    }
}