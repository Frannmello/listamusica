package br.com.listainkotlinmvp.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import br.com.listainkotlinmvp.data.model.Dado

@Dao
interface ObjectDao {

    @Query("SELECT * FROM  Dado")
    fun pegarDado():MutableList<Dado>

    @Insert
    fun adicionarDado(dado: Dado?)

    @Query("DELETE FROM Dado")
    fun deletarDado()

}