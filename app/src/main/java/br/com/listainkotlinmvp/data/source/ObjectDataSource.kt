package br.com.listainkotlinmvp.data.source

import br.com.listainkotlinmvp.data.model.Dado
import br.com.listainkotlinmvp.data.model.ObjectList

interface ObjectDataSource {
    fun pegarObjects(concentrador: Int,callbackResponse: CallbackResponse<ObjectList>)
    fun salvaDadosMedia(
        dado:ObjectList,
        sucesso: () -> Unit,
        erro: () -> Unit
    )
}