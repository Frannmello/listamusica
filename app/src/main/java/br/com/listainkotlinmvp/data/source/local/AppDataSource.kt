package br.com.listainkotlinmvp.data.source.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import br.com.listainkotlinmvp.data.model.Dado


@Database(
    entities = [
        Dado::class],
    version = 1,
    exportSchema = false
)
abstract class AppDataBase : RoomDatabase() {

    abstract fun objectDado(): ObjectDao

    companion object {

        private var INSTANCE: AppDataBase? = null

        private val lock = Any()

        fun getInstance(context: Context): AppDataBase {
            synchronized(lock) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDataBase::class.java,
                        "mediaList")
                        .build()
                }
                return INSTANCE!!
            }
        }
    }
}